package dam.androidruben.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import dam.androidruben.u3t3menuofactivities.model.Item;

public class ItemDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView tvNombre, tvVersion, tvApi, tvFecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setUI();
    }

    private void setUI() {
        imageView = findViewById(R.id.imageView);
        tvNombre = findViewById(R.id.tvNombre);
        tvVersion = findViewById(R.id.tvVersion);
        tvApi = findViewById(R.id.tvApi);
        tvFecha = findViewById(R.id.tvFecha);

        Item item = (Item) getIntent().getSerializableExtra("item");

        if (item != null) {
            imageView.setImageResource(item.getImage());
            tvNombre.setText(item.getNombre());
            tvVersion.setText("Version:" + item.getVersion());
            tvApi.setText("API:" + item.getApi());
            tvFecha.setText(item.getFecha());
        }

        imageView.setOnClickListener(v -> openWebsite(item));
    }

    private void openWebsite(Item item) {
        Uri webPage = Uri.parse(item.getUrl());
        Intent intent = new Intent(Intent.ACTION_VIEW, webPage);
        startActivity(intent);
    }
}