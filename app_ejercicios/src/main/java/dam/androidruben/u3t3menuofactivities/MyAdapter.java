package dam.androidruben.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidruben.u3t3menuofactivities.model.Item;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(Item item);
    }

    private ArrayList<Item> myDataSet;
    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvNombre, tvApi, tvVersion;

        public MyViewHolder(View view) {
            super(view);
            this.imageView = view.findViewById(R.id.imageViewCV);
            this.tvNombre = view.findViewById(R.id.tvNombreCV);
            this.tvApi = view.findViewById(R.id.tvApiCV);
            this.tvVersion = view.findViewById(R.id.tvVersionCV);
        }

        public void bind(Item item, OnItemClickListener listener) {
            this.imageView.setImageResource(item.getImage());
            this.tvNombre.setText(item.getNombre());
            this.tvVersion.setText("Version:" + item.getVersion());
            this.tvApi.setText("API:" + item.getApi());
            this.imageView.setOnClickListener(v -> listener.onItemClick(item));
        }
    }

    public MyAdapter(ArrayList<Item> myDataset, OnItemClickListener listener) {
        this.myDataSet = myDataset;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(parent.getContext())
                                                    .inflate(R.layout.card_view,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.bind(myDataSet.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }


}
