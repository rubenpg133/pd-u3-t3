package dam.androidruben.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

import dam.androidruben.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener, View.OnClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<Item> myDataset = new ArrayList<>();

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        recyclerView = findViewById(R.id.recyclerViewActivities);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MyAdapter(myDataset,this);
        new ItemTouchHelper(itemTouch).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(mAdapter);

        createItems();

        Button btAdd , btDelete, btRestore;
        btAdd = findViewById(R.id.btAdd);
        btDelete = findViewById(R.id.btDelete);
        btRestore = findViewById(R.id.btRestore);
        imageView = findViewById(R.id.ivVacio);

        btAdd.setOnClickListener(this);
        btDelete.setOnClickListener(this);
        btRestore.setOnClickListener(this);
    }

    private void createItems() {
        Item item1 = new Item();
        item1.setId(1);
        item1.setImage(R.drawable.android_12);
        item1.setNombre("Android 12");
        item1.setVersion("12.0");
        item1.setFecha("2021");
        item1.setApi("31");
        item1.setUrl("https://es.wikipedia.org/wiki/Android_12");
        myDataset.add(item1);
        Item item2 = new Item();
        item2.setId(2);
        item2.setImage(R.drawable.android_11);
        item2.setNombre("Android 11");
        item2.setVersion("11.0");
        item2.setFecha("2020");
        item2.setApi("30");
        item2.setUrl("https://es.wikipedia.org/wiki/Android_11");
        myDataset.add(item2);
        Item item3 = new Item();
        item3.setId(3);
        item3.setImage(R.drawable.android_10);
        item3.setNombre("Android 10");
        item3.setVersion("10.0");
        item3.setFecha("2019");
        item3.setApi("29");
        item3.setUrl("https://es.wikipedia.org/wiki/Android_10");
        myDataset.add(item3);
        Item item4 = new Item();
        item4.setId(4);
        item4.setImage(R.drawable.android_9);
        item4.setNombre("Android Pie");
        item4.setVersion("9.0");
        item4.setFecha("2018");
        item4.setApi("28");
        item4.setUrl("https://es.wikipedia.org/wiki/Android_Pie");
        myDataset.add(item4);
        Item item5 = new Item();
        item5.setId(5);
        item5.setImage(R.drawable.android_8);
        item5.setNombre("Android Oreo");
        item5.setVersion("8.0");
        item5.setFecha("2017");
        item5.setApi("26-27");
        item5.setUrl("https://es.wikipedia.org/wiki/Android_Oreo");
        myDataset.add(item5);
        Item item6 = new Item();
        item6.setId(6);
        item6.setImage(R.drawable.android_7);
        item6.setNombre("Android Nougat");
        item6.setVersion("7.0");
        item6.setFecha("2016");
        item6.setApi("24-25");
        item6.setUrl("https://es.wikipedia.org/wiki/Android_Nougat");
        myDataset.add(item6);
        Item item7 = new Item();
        item7.setId(7);
        item7.setImage(R.drawable.android_6);
        item7.setNombre("Android Marshmallow");
        item7.setVersion("6.0");
        item7.setFecha("2015");
        item7.setApi("23");
        item7.setUrl("https://es.wikipedia.org/wiki/Android_Marshmallow");
        myDataset.add(item7);
        Item item8 = new Item();
        item8.setId(8);
        item8.setImage(R.drawable.android_5);
        item8.setNombre("Android Lollipop");
        item8.setVersion("5.0");
        item8.setFecha("2014");
        item8.setApi("21-22");
        item8.setUrl("https://es.wikipedia.org/wiki/Android_Lollipop");
        myDataset.add(item8);
    }

    @Override
    public void onItemClick(Item item) {
        Intent intent = new Intent(this,ItemDetailActivity.class);
        intent.putExtra("item",item);
        startActivity(intent);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btAdd:
                Item item = new Item();
                item.setId(10);
                item.setImage(R.drawable.foto);
                item.setNombre("Nuevo");
                item.setVersion("1.0");
                item.setFecha("2022");
                item.setApi("35");
                item.setUrl("https://es.wikipedia.org/wiki/Android");
                myDataset.add(item);
                mAdapter.notifyDataSetChanged();
                recyclerView.smoothScrollToPosition(myDataset.size());
                imageView.setVisibility(View.INVISIBLE);
                break;
            case R.id.btDelete:
                myDataset.clear();
                mAdapter.notifyDataSetChanged();
                imageView.setVisibility(View.VISIBLE);
                break;
            case R.id.btRestore:
                myDataset.clear();
                createItems();
                mAdapter.notifyDataSetChanged();
                imageView.setVisibility(View.INVISIBLE);
                break;
        }
    }

    ItemTouchHelper.SimpleCallback itemTouch = new ItemTouchHelper.SimpleCallback(0,  ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            myDataset.remove(viewHolder.getAdapterPosition());
            mAdapter.notifyDataSetChanged();

            if (myDataset.isEmpty()){
                imageView.setVisibility(View.VISIBLE);
            } else {
                imageView.setVisibility(View.INVISIBLE);
            }
        }
    };
}